package heuritictp3

import heuritictp3.Genetic.{FitnessFunction, PlacementSolution, ProblemSolution}
import tools.Data.{PlacedTask, Proc, Task}
import tools.{CommonResolver, PerformanceTester, TaskCSVReader}

import scala.collection.immutable
import scala.util.Random

object Genetic {

  /**
    * Trait that all class representing a problem solution should implement.
    */
  trait ProblemSolution

  /**
    * We suppose that a list of task and proc is a solution by placing them in the order that they are given.
    * The only thing that change between solutions is the order of tasks.
    */
  case class PlacementSolution(tasks: List[Task], proc: List[Proc]) extends ProblemSolution

  /**
    * Represent the type that all fitness function should have. Mainly used to write code more concise.
    * Fitness functions link a [[ProblemSolution]] with a score, represented by an [[Int]] value.
    *
    * @tparam T The type of the solution, it have to be a subtype of [[ProblemSolution]]
    */
  type FitnessFunction[T <: ProblemSolution] = T => Int

  /**
    * Represent the type that all selection function should have. Mainly used to write code more concise.
    * Selection function is used to delete some elements of a given population. It takes a [[FitnessFunction]] as
    * parameter to choose the score on which the elements are selected.
    *
    * @tparam T The type of the solution, it have to be a subtype of [[ProblemSolution]]
    */
  type SelectionFunction[T <: ProblemSolution] = (FitnessFunction[T], List[T]) => List[T]

  /**
    * Represent the type that all generate function should have. Mainly used to write code more concise.
    * Generate function is used to create elements from a given population.
    *
    * @tparam T The type of the solution, it have to be a subtype of [[ProblemSolution]]
    */
  type GenerateFunction[T <: ProblemSolution] = List[T] => List[T]

  def main(args: Array[String]): Unit = {
    //We create solver choosing selection, mutation and fitness function
    def simpleGenetic: (List[PlacementSolution], Int) => PlacementSolution =
      resolveWithGenetic[PlacementSolution](_: List[PlacementSolution], _: Int)(
        FitnessFunctions.totalDurationFitness,
        SelectionFunctions.selectionBestFitMoreChances,
        GenerateFunctions.moreMutationGeneration)

    val nbProc = 8
    //Generating random occurrence of the problem to test multiple times on different occurrences.
    val inputProblem: immutable.Seq[List[Task]] = for(_ <- 1 to 10) yield PerformanceTester.generateTaskDataSet(300, nbProc/2, 30, seed = 1)

    //Get solution for same problems increasing the number of generation
    for (i <- 1 to 30) {
      //We have to wrap simple genetic method in a method that PerformanceTester can take as argument
      def geneticFunctionResolve(proc: List[Proc], list: List[Task]): List[PlacedTask] = {
        val fullRandomBasePopulation: Seq[PlacementSolution] = for(_ <- 1 to 10) yield PlacementSolution(Random.shuffle(list), proc)
        val placementSolution = simpleGenetic(fullRandomBasePopulation.toList, i)
        CommonResolver.getPlacement(placementSolution.tasks, placementSolution.proc)
      }

      TaskCSVReader.writeResult("/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/genetic.csv", geneticFunctionResolve(CommonResolver.generateProcs(8), inputProblem.head))

      PerformanceTester.writeResultInCSV(geneticFunctionResolve, i.toString, inputProblem.toList, "/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/results.csv", nbProc)
    }

  }

  /**
    * Resolve a problem with genetic method.
    *
    * @param basePopulation All elements that compose the base population.
    * @param nbGeneration   Number of generation that the method will generate before giving the best solution found.
    * @param generate       This function have to generate the same proportion of people that the selection one does.
    * @return The best element found after nbGeneration.
    */
  def resolveWithGenetic[T <: ProblemSolution](basePopulation: List[T],
                                               nbGeneration: Int)
                                              (fitness: FitnessFunction[T],
                                               selection: SelectionFunction[T],
                                               generate: GenerateFunction[T]): T =
    nbGeneration match {
      case 0 => basePopulation minBy fitness
      case n => resolveWithGenetic(generate(selection(fitness, basePopulation)), n - 1)(fitness, selection, generate)
    }
}


object FitnessFunctions {
  def totalDurationFitness: FitnessFunction[PlacementSolution] = (problem: PlacementSolution) =>
    PerformanceTester.endTime(CommonResolver.getPlacement(problem.tasks, problem.proc))
}

object SelectionFunctions {
  def selectionHalfBestWin[T <: ProblemSolution](fitnessFunction: FitnessFunction[T], solutions: List[T]): List[T] =
    solutions.sortBy(solution => fitnessFunction(solution)) take (solutions.size / 2)

  def selectionBestFitMoreChances[T <: ProblemSolution](fitnessFunctions: FitnessFunction[T], solutions: List[T]): List[T] = {
    val bestFitness: Int = fitnessFunctions(solutions minBy fitnessFunctions)
    val sortedSolutions = solutions sortBy (sol => (fitnessFunctions(sol) - bestFitness) * (Math.random / 2 + 0.5))
    sortedSolutions take (sortedSolutions.size / 2)
  }
}

object GenerateFunctions {

  def moreMutationGeneration(solutions: List[PlacementSolution]): List[PlacementSolution] =
    solutions.flatMap(solution => List(solution, tripleMutation(solution)))

  def mutationGeneration(solutions: List[PlacementSolution]): List[PlacementSolution] =
    solutions.flatMap(solution => List(solution, mutateSolutionRandomly(solution)))

  def tripleMutation(solution: PlacementSolution): PlacementSolution =
    mutateSolutionRandomly(mutateSolutionRandomly(mutateSolutionRandomly(solution)))

  def mutateSolutionRandomly(solution: PlacementSolution): PlacementSolution = {
    val mutateId: Int = (Math.random * (solution.tasks.size - 1)).toInt
    solution.copy(mutateListAt(solution.tasks, mutateId))
  }

  def mutateListAt[T](list: List[T], n: Int): List[T] = {
    assert(n <= list.size - 2)
    val (part1, part2) = list.splitAt(n)
    part1 ++ List(part2.tail.head, part2.head) ++ part2.tail.tail
  }
}