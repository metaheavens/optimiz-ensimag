package heuritictp3

import java.io.File

import com.github.tototoshi.csv.CSVWriter
import tools.Data.{PlacedTask, Proc, Task}
import heuristic.Main.nbProcOrderededReverseAndTimeOrderedReverseSolution
import tools.{CommonResolver, PerformanceTester}

import scala.collection.mutable


object Main {
  def main(args: Array[String]): Unit = {

    val outputPath = "/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/results.csv"

    val writer: CSVWriter = CSVWriter.open(new File(outputPath), append = true)
    writer.writeRow(List("Exp_name", "execution_time", "wait_avg", "usage_avg", "end_time"))
    writer.close()

    for(i <- 7 to 11){
      val tests = List(PerformanceTester.generateTaskDataSet(i, 8, 30))
      PerformanceTester.writeResultInCSV(bruteForceBranchRemoving, "elaging" + i, tests, outputPath, 8)
      PerformanceTester.writeResultInCSV(brutforce.Main.resolverOptimized, "brutForce" + i, tests, outputPath, 8)
      println(i + " DONE !")
    }

  }

  /**
    * Make a brute force removing useless branches found by comparing the actual best solution with the actual solution
    */
  def bruteForceBranchRemoving(proc: List[Proc], task: List[Task]): List[PlacedTask] = {
    val heuristicSolution = nbProcOrderededReverseAndTimeOrderedReverseSolution(proc, task)
    recursiveBruteForce(proc, List(), task.toBuffer, heuristicSolution)
  }

  /**
    * Recursive function that found the best result of the problem.
    * It keeps the best solution to compare it with the actual solution.
    */
  def recursiveBruteForce(proc: List[Proc],
                          choseTask: List[Task],
                          taskToChoose: mutable.Buffer[Task],
                          bestSolution: List[PlacedTask]): List[PlacedTask] = {
    var bestSol = bestSolution

    if(taskToChoose.isEmpty) {
      bestSol = List(bestSol, CommonResolver.getPlacement(choseTask, proc)).minBy(PerformanceTester.endTime)
    }
    else {
      for (i <- taskToChoose.indices) {
        val tmpElem = taskToChoose.remove(i)

        val middleSolution = CommonResolver.getPlacement(tmpElem +: choseTask, proc)
        val middleSolutionTime = PerformanceTester.endTime(middleSolution)

        if(middleSolutionTime < PerformanceTester.endTime(bestSol))
          bestSol = recursiveBruteForce(proc, tmpElem +: choseTask, taskToChoose, bestSol)

        taskToChoose.insert(i, tmpElem)
      }
    }

    bestSol
  }

}
