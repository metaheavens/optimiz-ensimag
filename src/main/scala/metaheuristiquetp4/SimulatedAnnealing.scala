package metaheuristiquetp4

import java.io.File

import com.github.tototoshi.csv.{CSVReader, CSVWriter}
import tools.Data.{PlacedTask, Proc, Task}
import tools.{CommonResolver, PerformanceTester, TaskCSVReader}

import scala.util.Random

object SimulatedAnnealing {

  //val writer: CSVWriter = CSVWriter.open(new File("/home/pamartn/result.csv"))
  var datas: List[List[Int]] = (for(i <- 1 to 100) yield List[Int]()).toList
  var curr_ite = 0

  val rdm = new Random()
  var count_ok: Int = 0
  var count_ko: Int = 0

  var optimumPlacedTask: List[PlacedTask] = List[PlacedTask]()

  def generateRandomNeighbor(tasks: List[Task], permutCount : Int): List[Task] = {
    if(permutCount > 0)
      generateRandomNeighbor(tasks, permutCount-1)

    val mutateId: Int = (Math.random * (tasks.size - 1)).toInt
    mutateListAt(tasks, mutateId)
  }

  def mutateListAt[T](list: List[T], n: Int): List[T] = {
    assert(n <= list.size - 2)
    val (part1, part2) = list.splitAt(n)
    part1 ++ List(part2.tail.head, part2.head) ++ part2.tail.tail
  }


  def simulatedAnnealing(tasks: List[Task], procs: List[Proc], temp: Double, tempMin: Double, nbIter: Int, initialCost: Int): List[Task] ={
    //System.out.print(initialCost + ", ")
    datas = datas.updated(curr_ite, List(initialCost) ++ datas(curr_ite))
    if(nbIter == 0) {
      if(temp < tempMin) {
        return tasks
      }
      else {
        simulatedAnnealing(tasks, procs, temp*(1- 0.002), tempMin, 10, initialCost)
      }
    } else {
      val neighbor = generateRandomNeighbor(tasks, rdm.nextInt((tasks.length*0.1).toInt)+1)
      val placedTask = CommonResolver.getPlacement(tasks, procs)
      val neighborCost = PerformanceTester.endTime(placedTask)

      val acceptanceProbability = Math.exp((initialCost - neighborCost) / temp)

      if (neighborCost < initialCost || acceptanceProbability > Math.random()) {
        count_ok = count_ok + 1
        /*
          Keep track of the best time in all iterations
         */
        if(optimumPlacedTask == null || PerformanceTester.endTime(optimumPlacedTask) > neighborCost)
          optimumPlacedTask = placedTask
        simulatedAnnealing(neighbor, procs, temp, tempMin, nbIter - 1, neighborCost)
      } else {
        count_ko = count_ko + 1
        simulatedAnnealing(tasks, procs, temp, tempMin, nbIter - 1, initialCost)
      }
    }
  }

  def solution(procs: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    val cost = PerformanceTester.endTime(CommonResolver.getPlacement(tasks, procs))
    CommonResolver.getPlacement(simulatedAnnealing(tasks, procs, 10.0, 0.01, 10, cost), procs)
  }

  def resolutionFromFile(inputFilename: String, outputFilename: String) = {
    val reader: CSVReader = CSVReader.open(new File(inputFilename))
    val values: List[Task] = reader.all.tail map (list => Task(list(0).toInt, list(1).toInt, list(2).toInt))
    //System.out.print("best time: " + PerformanceTester.endTime(optimumPlacedTask)  +  " | accepted : " + count_ok + " | refused : " + count_ko )
    //TaskCSVReader.writeResult(inputFilename, optimumPlacedTask)
  }

  def performanceTest() = {
    val nbProc = 30;
    for(i <- 1 to 3) {
      val sol = solution(CommonResolver.generateProcs(nbProc), PerformanceTester.generateTaskDataSet(50, nbProc, 5, seed = 1))
      System.out.print("accepted : " + count_ok + " | refused : " + count_ko )
      System.out.println("change")
      //writer.writeRow(datas(curr_ite).reverse)
      curr_ite += 1
    }
  }

  def main(args: Array[String]): Unit = {
    resolutionFromFile("/home/metaheavens/Documents/ensimag/optimiz-ensimag/input/medium.csv", "/home/pamartn/result.csv")
    //performanceTest()
  }

}
