package tools

import java.io.File

import com.github.tototoshi.csv.{CSVReader, CSVWriter}
import tools.Data.{PlacedTask, Task}

object TaskCSVReader {

  def readTasks(path: String): List[Task] = {
    val reader: CSVReader = CSVReader.open(new File(path))
    val values: List[Task] = reader.all.tail map (list => Task(list(0).toInt, list(1).toInt, list(2).toInt))
    reader.close()
    values
  }

  def writeResult(path: String, tasks: List[PlacedTask]): Unit = {
    val writer: CSVWriter = CSVWriter.open(new File(path))
    val toWrite: List[List[String]] = tasks.map(task => List(task.task.jobId, task.task.nbProcNeeded, task.task.time, task.startingTime, task.allocatedProc.mkString(" ")).map(_.toString))
    writer.writeRow(List("job_id","requested_number_of_processors","execution_time","starting_time","allocated_processors"))
    toWrite.foreach(list => writer.writeRow(list))
    writer.close()
  }

}
