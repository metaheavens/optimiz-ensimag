package tools

object Data {
  case class Task(jobId: Int, nbProcNeeded: Int, time: Int)
  case class PlacedTask(task: Task, startingTime: Int, allocatedProc: List[Int])

  case class Proc(procId: Int,
                  nextTimeAvailable: Int = 0,
                  availableInterval: Seq[Interval] = Seq(Interval(0, Int.MaxValue))) {
    def removeInterval(interval: Interval): Proc = copy(availableInterval = availableInterval.flatMap(_ - interval))
  }
}
