package tools

import java.io.File

import com.github.tototoshi.csv.CSVWriter
import tools.Data.{PlacedTask, Proc, Task}

import scala.collection.mutable
import scala.util.Random

object PerformanceTester {

  /**
    * Return a random solution to compare it with heuristics
    */
  def randomSolution(procs: List[Proc], task: List[Task]): List[PlacedTask] =
    CommonResolver.getPlacement(Random.shuffle(task), procs)

  /**
    * Return the lower bound of the solution of the placement problem
    */
  def lowerBound(procs: List[Proc], task: List[Task]): Double =
    task.map(task => task.nbProcNeeded * task.time).sum / procs.length

  /**
    * Return the wait average that task have waited before be placed in the solution passed as parameter
    */
  def waitAverage(tasks: List[PlacedTask]): Double = tasks.map(_.startingTime).sum.toDouble / tasks.length

  /**
    * Return the time when all task are finished in the solution passed as parameter
    */
  def endTime(tasks: List[PlacedTask]): Int = tasks.map(task => task.startingTime + task.task.time).max

  /**
    * Return the usage percentage of procs for the solution passed as parameter
    */
  def percentageUsageAverage(tasks: List[PlacedTask]): Double = {
    val sumByProc: mutable.Map[Int, Int] = mutable.Map[Int, Int]()
    tasks.foreach{ task =>
        task.allocatedProc.foreach(procId => sumByProc.put(procId, sumByProc.getOrElse(procId, 0) + task.task.time))
    }

    val totalUsedTimes = sumByProc.values
    val avgTotalUsedTimes = totalUsedTimes.sum.toDouble / totalUsedTimes.size
    (avgTotalUsedTimes / endTime(tasks).toDouble) * 100
  }

  /**
    * This big function will launch solver function and test it to find how it is
    */
  def writeResultInCSV(solverFunction: (List[Proc], List[Task]) => List[PlacedTask],
                       solverName: String,
                       listTests: List[List[Task]],
                       pathResults: String,
                       numberCPU: Int): Unit = {
    val expResults: Seq[ExpResult] = generateExpResult(solverFunction, listTests, numberCPU)
    val expResultAvg = avgExpResult(expResults)

    val writer: CSVWriter = CSVWriter.open(new File(pathResults), append = true)
    val expText = List(solverName, expResultAvg.executionTime, expResultAvg.waitAvg, expResultAvg.usageAvg, expResultAvg.endTime)

    writer.writeRow(expText)
    writer.close()
  }

  private def avgExpResult(expResults: Seq[ExpResult]) = {
    ExpResult(
      expResults.map(_.executionTime).sum / expResults.size,
      expResults.map(_.waitAvg).sum / expResults.size,
      expResults.map(_.usageAvg).sum / expResults.size,
      expResults.map(_.endTime).sum / expResults.size
    )
  }

  private def generateExpResult(solverFunction: (List[Proc], List[Task]) => List[PlacedTask], listTests: List[List[Task]], numberCPU: Int) = {
    for {
      tasks <- listTests
      cpu = CommonResolver.generateProcs(numberCPU)
      t1 = System.nanoTime
      result = solverFunction(cpu, tasks)
      t2 = System.nanoTime
      end = endTime(result)
      waitAvg = waitAverage(result)
      usage = percentageUsageAverage(result)
    } yield ExpResult((t2 - t1).toDouble * 0.000000001, waitAvg, usage, end)
  }

  //This struct store the result of one experience. Execution time is stored in seconds
  case class ExpResult(executionTime: Double, waitAvg: Double, usageAvg: Double, endTime: Double)

  //Generate a new task dataset
  def generateTaskDataSet(taskNumber: Int, coreNb: Int, maxTime: Int, seed: Int = 0): List[Task] = {
    val randomGenerator = new Random(seed)
    var taskList: List[Task] = List()

    for (a <- 1 to taskNumber){
      val nbProcNeeded = randomGenerator.nextInt(coreNb)+1
      val time = randomGenerator.nextInt(maxTime) + 1

      taskList = taskList :+ Task(a-1, nbProcNeeded, time)
    }
    taskList
  }

  //task dataset generation test
  def main(args: Array[String]): Unit = for (a <- generateTaskDataSet(1000, 32, 100)) println(a)
}