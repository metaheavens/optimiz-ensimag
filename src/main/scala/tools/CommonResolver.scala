package tools

import java.io.File

import com.github.tototoshi.csv.CSVReader
import tools.Data.{PlacedTask, Proc, Task}


object CommonResolver {

  def main(args: Array[String]): Unit = {
    val reader: CSVReader = CSVReader.open(new File("/home/metaheavens/Documents/ensimag/optimiz-ensimag/input/medium.csv"))
    val values: List[Task] = reader.all.tail map (list => Task(list(0).toInt, list(1).toInt, list(2).toInt))

    val results: Seq[PlacedTask] = getPlacementBackFilling(values, generateProcs(32))
    TaskCSVReader.writeResult("/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/smallBackFill.csv", results.toList)

    val res2 = getPlacement(values, generateProcs(32))
    TaskCSVReader.writeResult("/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/smallNoBackFill.csv", res2)
  }

  /**
    * Thanks an order and a list of procs, return a list of PlacedTask
    */
  def getPlacement(order: List[Task], procs: List[Proc]): List[PlacedTask] =
  // FoldLeft is used to iter on each element of a list. It as an accumulator that is initialize (with procs and an empty list).
  // The high order function in foldLeft have to return the accumulator for the next iteration, and take as parameter
  // the old accumulator (procs and placedTasks) and the new element to add to the accumulator (task).
    order.foldLeft((procs, List[PlacedTask]())) {
      case ((procs: Seq[Proc], placedTasks: Seq[PlacedTask]), task: Task) =>
        val (newProcs, newPlacedTask) = placeTask(procs, task)
        (newProcs, newPlacedTask :: placedTasks)
    }._2

  /**
    * This function place a task in function of available procs
    *
    * @return A refreshed list of procs
    */
  def placeTask(procs: List[Proc], task: Task): (List[Proc], PlacedTask) = {
    val sortedProcs = procs.sortBy(_.nextTimeAvailable) //Sort all procs by it availability
    val (usedProc, unusedProc) = sortedProcs.splitAt(task.nbProcNeeded) //Separate procs that we will use for the task
    val startTaskTime = usedProc.last.nextTimeAvailable
    val usedProcWithNewAvailableTime = usedProc.map(proc => proc.copy(nextTimeAvailable = startTaskTime + task.time))
    val placedTask = PlacedTask(task, startTaskTime, usedProc.map(_.procId))
    (usedProcWithNewAvailableTime ++ unusedProc, placedTask)
  }

  def generateProcs(nbProc: Int): List[Proc] = (1 to nbProc) map (id => Proc(id)) toList

  /**
    * This function place a task in function of available procs. It does it better than placeTask because it use
    * [[ProcessorManagers]] that place task in gaps when it can.
    */
  def getPlacementBackFilling(order: List[Task], procs: List[Proc]): List[PlacedTask] =
    order.foldLeft((new ProcessorManagers(procs), List[PlacedTask]())) {
      case ((manager: ProcessorManagers, placedTasks: Seq[PlacedTask]), task: Task) =>
        val (updatedManager, placedTask) = manager.placeTask(task)
        (updatedManager, placedTask :: placedTasks)
    }._2

}

/**
  * Represents a pool of [[Proc]] associated with a [[GroupedResourcesIntervals]] that allows to do some complexe
  * back filling operation on task placement in a reasonable time.
  */
case class ProcessorManagers(procs: Seq[Proc], groupedResourcesIntervals: GroupedResourcesIntervals) {

  def this(procs: Seq[Proc]) = this(procs, GroupedResourcesIntervals(Seq((Interval(0, Int.MaxValue), procs.length))))

  /**
    * Separate [[Proc]] in two lists, one with [[Proc]] that are available at this time, the others are not.
    */
  private def partitionAvailableProc(interval: Interval): (Seq[Proc], Seq[Proc]) =
    procs.partition(proc => proc.availableInterval.exists(_ contains interval))

  /**
    * Place a task using both list of [[Proc]] and [[GroupedResourcesIntervals]]. It does some backfilling
    * without calculating cartesian product of all available gaps in list of [[Proc]].
    */
  def placeTask(task: Task): (ProcessorManagers, PlacedTask) =
    groupedResourcesIntervals.fit(task.time, task.nbProcNeeded) match {
      case Some(interval) =>
        val (usedProc, notUsedProc) = removeIntervalInProcs(interval, task.nbProcNeeded)
        usedProc match {
          case list: Seq[Proc] if list.isEmpty => fallBackNoProc(task)
          case _: Seq[Proc] =>
            val newGroupedResources = groupedResourcesIntervals.removeInterval(interval, task.nbProcNeeded)
            val newManager = ProcessorManagers(usedProc ++ notUsedProc, newGroupedResources)
            val placedTask = PlacedTask(task, interval.lowerBound, usedProc.map(_.procId).toList)
            (newManager, placedTask)
        }
      case None => throw new Exception("impossible to fit task: " + task)
    }

  /**
    * This method is called when there is inconsistencies between the list of [[Proc]] and [[GroupedResourcesIntervals]].
    * It remove the [[Interval]] of [[GroupedResourcesIntervals]] that is not really available in the list of [[Proc]]
    * and place the task at the end of the schedule.
    */
  private def fallBackNoProc(task: Task): (ProcessorManagers, PlacedTask) = {
    val orderedProcs = this.procs
      .map(proc => proc.copy(availableInterval = proc.availableInterval.sortBy(i => i.upperBound).reverse))
      .sortBy(proc => proc.availableInterval.head.lowerBound)

    val startTime = orderedProcs.splitAt(task.nbProcNeeded)._1.map(_.availableInterval.head.lowerBound).min
    val (usedProc, notUsed) = removeIntervalInProcs(Interval(startTime, startTime + task.time), task.nbProcNeeded)
    val newGroupedResources = groupedResourcesIntervals.removeInterval(Interval(startTime, startTime + task.time), task.nbProcNeeded)
    val newManager = ProcessorManagers(usedProc ++ notUsed, newGroupedResources)
    (newManager, PlacedTask(task, startTime, usedProc.map(_.procId).toList))
  }

  /**
    * Returns 2 seq of [[Proc]]. The first one is modified processors by the remove operation,
    * the others are not changed.
    */
  private def removeIntervalInProcs(interval: Interval, nbProc: Int): (Seq[Proc], Seq[Proc]) =
    partitionAvailableProc(interval) match {
      case (available, notAvailable) if procs.length >= nbProc =>
        val (used, notUsed) = available.splitAt(nbProc)
        (used.map(proc => proc removeInterval interval), notUsed ++ notAvailable)
      case _ => throw new Exception("not enough proc to place interval: " + interval + s" for $nbProc procs")
    }
}

/**
  * Represents an interval. lowerBound is included, uppedBound is not.
  */
case class Interval(lowerBound: Int, upperBound: Int) {

  def contains(i: Interval): Boolean = lowerBound <= i.lowerBound && upperBound >= i.upperBound

  def minus(i: Interval): List[Interval] =
    List(intersection(Interval(i.upperBound, Int.MaxValue)), intersection(Interval(Int.MinValue, i.lowerBound))).flatten

  def -(i: Interval): List[Interval] = minus(i)

  def intersection(i: Interval): Option[Interval] =
    (Math.max(lowerBound, i.lowerBound), Math.min(upperBound, i.upperBound)) match {
      case (a, b) if a < b => Some(Interval(a, b))
      case _ => None
    }

  def merge(i: Interval): Interval =
    mergeOption(i).getOrElse(throw new Exception("unmergable intervals tried to be merged"))

  def hasCommonBound(i: Interval): Boolean = i.lowerBound == upperBound || lowerBound == i.upperBound

  def mergeOption(i: Interval): Option[Interval] =
    if(intersection(i).isDefined || hasCommonBound(i))
      Some(Interval(Math.min(lowerBound, i.lowerBound), Math.max(upperBound, i.upperBound)))
    else None

  def length: Int = upperBound - lowerBound

  override def toString: String = s"Interval($lowerBound, $upperBound)"
}

/**
  * This class represents the availability of resources during time. Represented by a list of intervals associated with
  * a value, that represents the quantity of resources available during this time.
  */
case class GroupedResourcesIntervals(summedIntervals: Seq[(Interval, Int)]) {
  val sortedIntervals: Seq[(Interval, Int)] = summedIntervals.sortBy(interval => interval._1.lowerBound)

  /**
    * Returns new [[GroupedResourcesIntervals]] removing the quantity of resources asked during the interval.
    */
  def removeInterval(interval: Interval, nbRemove: Int): GroupedResourcesIntervals = {
    val (crossedIntervals, others) = summedIntervals.partition(inter => inter._1.intersection(interval).isDefined)
    GroupedResourcesIntervals(mergeLinkedIntervals(crossedIntervals.flatMap(removeInterval(interval, nbRemove, _)) ++ others))
  }

  private def removeInterval(interval: Interval,
                             nbRemove: Int,
                             intervalToCut: (Interval, Int)): Seq[(Interval, Int)] = {
    val notChanged: Seq[(Interval, Int)] = intervalToCut._1 - interval map (i => (i, intervalToCut._2))
    val changed: Interval = intervalToCut._1.intersection(interval)
      .getOrElse(throw new Exception("Error removing Interval"))
    if (intervalToCut._2 < nbRemove) throw new Exception("trying to remove to much")
    (changed, intervalToCut._2 - nbRemove) +: notChanged
  }

  private def mergeLinkedIntervals(intervals: Seq[(Interval, Int)]): Seq[(Interval, Int)] = {
    val sortedIntervals = intervals.sortBy(_._1.lowerBound)
    sortedIntervals.tail.foldLeft(Seq(sortedIntervals.head)) {
      (a: Seq[(Interval, Int)], b: (Interval, Int)) =>
        if (a.head._2 == b._2) (a.head._1 merge b._1, a.head._2) +: a.tail
        else b +: a
    }
  }

  /**
    * Returns the first interval that is available and match with the requested time/value pair given as parameters.
    */
  def fit(intervalLength: Int, value: Int): Option[Interval] =
    filterMergedByResource(value).sortBy(_.lowerBound).find(_.length >= intervalLength)
      .map(interval => Interval(interval.lowerBound, interval.lowerBound + intervalLength))

  private def filterMergedByResource(value: Int): Seq[Interval] = {
    val filtered: Seq[Interval] = sortedIntervals.filter(_._2 >= value).map(_._1)
    if(filtered.isEmpty) throw new Exception(s"not enough resources for $value procs")
    filtered.tail.foldLeft(Seq[Interval](filtered.head)) {
      (a: Seq[Interval], b: Interval) =>
        a.head mergeOption b match {
          case Some(merged) => merged +: a.tail
          case None => b +: a
        }
    }
  }
}
