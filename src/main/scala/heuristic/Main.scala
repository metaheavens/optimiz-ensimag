package heuristic

import java.io.File

import com.github.tototoshi.csv.CSVWriter
import tools.Data.{PlacedTask, Proc, Task}
import tools.{CommonResolver, PerformanceTester, TaskCSVReader}

object Main {
  def main(args: Array[String]): Unit = {

    val outputPath = "/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/results.csv"
    val inputPath = "/home/metaheavens/Documents/ensimag/optimiz-ensimag/input/medium.csv"
    val writer: CSVWriter = CSVWriter.open(new File(outputPath), append = true)
    writer.writeRow(List("Exp_name", "execution_time", "wait_avg", "usage_avg", "end_time"))
    writer.close()

    /* TODO to refactor to take random input
    PerformanceTester.writeResultInCSV(PerformanceTester.randomSolution, "RANDOM", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(timeOrderedSolution, "TIME_ORDERED", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(timeOrderedSolution, "TIME_ORDERED_REVERSE", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(nbProcOrderedSolution, "NB_PROC_ORDERED", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(nbProcOrderedSolutionReverse, "NB_PROC_ORDERED_REVERSE", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(sizeOrderedSolution, "SIZE_ORDERED", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(sizeOrderedSolutionReverse, "SIZE_ORDERED_REVERSE", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(nbProcOrderededReverseAndTimeOrderedSolution, "NB_PRO_ORDERED_REVERSE_AND_TIME_ORDERED", inputPath, outputPath, 32)
    PerformanceTester.writeResultInCSV(nbProcOrderededReverseAndTimeOrderedReverseSolution, "NB_PRO_ORDERED_REVERSE_AND_TIME_REVERSE_ORDERED", inputPath, outputPath, 32)
    */

    TaskCSVReader.writeResult("/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/medium.csv",
      nbProcOrderededReverseAndTimeOrderedReverseSolution(CommonResolver.generateProcs(32), TaskCSVReader.readTasks(inputPath)))

  }

  def timeOrderedSolution(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    CommonResolver.getPlacement(tasks.sortBy(task => task.time), proc)
  }

  def timeOrderedSolutionReverse(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    CommonResolver.getPlacement(tasks.sortBy(task => task.time).reverse, proc)
  }

  def nbProcOrderedSolution(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    CommonResolver.getPlacement(tasks.sortBy(task => task.nbProcNeeded), proc)
  }

  def nbProcOrderedSolutionReverse(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    CommonResolver.getPlacement(tasks.sortBy(task => task.nbProcNeeded).reverse, proc)
  }

  def sizeOrderedSolution(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    CommonResolver.getPlacement(tasks.sortBy(task => task.nbProcNeeded * task.time), proc)
  }

  def sizeOrderedSolutionReverse(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    CommonResolver.getPlacement(tasks.sortBy(task => task.nbProcNeeded * task.time).reverse, proc)
  }

  def nbProcOrderededReverseAndTimeOrderedSolution(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    val tasksGrouppedAndSubOrdered: Map[Int, List[Task]] =
      tasks.groupBy(task => task.nbProcNeeded) //Regroup by nbProc
      .map(elem => (elem._1, elem._2.sortBy(task => task.time))) //Each sub list is sort by time

    val orderedTasks = tasksGrouppedAndSubOrdered.toList.sortBy(elem => elem._1).reverse.flatMap(elem => elem._2)
    CommonResolver.getPlacement(orderedTasks, proc)
  }

  def nbProcOrderededReverseAndTimeOrderedReverseSolution(proc: List[Proc], tasks: List[Task]): List[PlacedTask] = {
    val tasksGrouppedAndSubOrdered: Map[Int, List[Task]] =
      tasks.groupBy(task => task.nbProcNeeded) //Regroup by nbProc
        .map(elem => (elem._1, elem._2.sortBy(task => task.time).reverse)) //Each sub list is sort by time

    val orderedTasks = tasksGrouppedAndSubOrdered.toList.sortBy(elem => elem._1).reverse.flatMap(elem => elem._2)
    CommonResolver.getPlacement(orderedTasks, proc)
  }
}
