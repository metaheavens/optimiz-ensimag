package brutforce

import java.io.File

import com.github.tototoshi.csv._
import tools.CommonResolver
import tools.Data.{PlacedTask, Proc, Task}


object Main {
  def main(args: Array[String]): Unit = {
    val reader: CSVReader = CSVReader.open(new File("/home/metaheavens/Documents/ensimag/optimiz-ensimag/input/small.csv"))
    val writer: CSVWriter = CSVWriter.open(new File("/home/metaheavens/Documents/ensimag/optimiz-ensimag/output/tiny.csv"))
    val t1: Long = System.nanoTime
    val values: List[Task] = reader.all.tail map (list => Task(list(0).toInt, list(1).toInt, list(2).toInt))

    /*val allOrders = getAllOrders(values)

    val t2 = System.nanoTime

    val placements = getPlacements(allOrders, generateProcs(4))

    val t3 = System.nanoTime

    val solution = getBestPlacements(placements)

    val t4: Long = System.nanoTime

    println("All orders: %.5fs".format((t2 - t1).toDouble * 0.000000001))
    println("All placements: %.5fs".format((t3 - t2).toDouble * 0.000000001))
    println("Total: %.5fs".format((t4 - t1).toDouble * 0.000000001))

    writePlacedTasks(solution, writer)*/

    val solution = resolverOptimized(CommonResolver.generateProcs(4), values)

    writePlacedTasks(solution, writer)

    val t2 = System.nanoTime

    println("Total: %.5fs".format((t2 - t1).toDouble * 0.000000001))

    reader.close()
    writer.close()
  }

  def getBestPlacements(placements: List[List[PlacedTask]]): List[PlacedTask] =
    placements.map(placement => placement.sortBy(task => task.task.time + task.startingTime))
      .minBy(placement => placement.last.startingTime + placement.last.task.time)

  /**
    * For a given list, return all possible permutations with its elements
    */
  def getAllOrders[T](_list: List[T]): List[List[T]] = _list match {
    case head :: Nil => List(List(head)) // Case when left only one element, there is only one possibility

    case list => list.flatMap { elem => //(FlatMap will change the List[List[List[T]]] in List[List[T]] merging them)
      // For each element
      val listWithoutActualElem = removeFirstEl(list, elem) // Get the list without this element
      val allSubOrders = getAllOrders(listWithoutActualElem) // Generate recursively all possible orders of this sublist
      allSubOrders.map(innerList => elem :: innerList) // Append the removed element to all generated list and return it
      // In Scala there is no need of the return key word, the last statement will be returned
    }
  }

  def resolverOptimized(procs: List[Proc], list: List[Task]): List[PlacedTask] = {
    var bestOne: List[PlacedTask] = null

    var i = 0
    while(i < fact(list.size).toInt) {
      val order = writeNumberAsOrder(i, list.size)
      val taskOrder = orderToTaskOrder(order, list)
      val placedTasks = CommonResolver.getPlacement(taskOrder, procs)
      if (bestOne != null) bestOne = getBestPlacements(List(bestOne, placedTasks)) else bestOne = placedTasks
      i = i + 1
    }

    bestOne
  }

  def orderToTaskOrder(order: List[Int], task: List[Task]): List[Task] = {
    val taskBuffer = task.toBuffer
    order.foldLeft(List[Task]()){
      case (accumulator, elemToTake) => taskBuffer.remove(elemToTake) :: accumulator
    }
  }

  /**
    * Return all placements found for each order passed as parameter.
    */
  def getPlacements(allPossibilities: List[List[Task]], procs: List[Proc]): List[List[PlacedTask]] =
    allPossibilities.map(possibility => CommonResolver.getPlacement(possibility, procs))

  /**
    * This method remove the first element in the given list. List are immutable, so a new list is returned.
    *
    * @return List with the remove element.
    */
  def removeFirstEl[T](list: List[T], el: T): List[T] = {
    val lists: (List[T], List[T]) = list.span(elem => elem != el)
    // span method of list split the list in two parts at the first element where the condition is not complete

    lists._1 ++ lists._2.tail // lists._2.tail refers to the second part of the list without the matched element
  }

  def writePlacedTasks(tasks: List[PlacedTask], writer: CSVWriter): Unit = {
    val toWrite: List[List[String]] = tasks.map(task => List(task.task.jobId, task.task.nbProcNeeded, task.task.time, task.startingTime, task.allocatedProc.mkString(" ")).map(_.toString))
    writer.writeRow(List("job_id","requested_number_of_processors","execution_time","starting_time","allocated_processors"))
    toWrite.foreach(list => writer.writeRow(list))
  }

  def writeNumberAsOrder(number: Long, listSize: Int): List[Int] = {
    val dividers: Seq[Int] = for(i <- 1 to numberOfElements(number)) yield fact(i).toInt
    val result = dividers.reverse.foldLeft((List[Int](), number)){
      case ((list: List[Int], actualNumber: Long), divider: Int) =>
        ((actualNumber / divider).toInt :: list, actualNumber % divider)
    }._1.reverse
    ((1 until listSize - result.size).map(_ => 0) ++ result ++ List(0)).toList
  }

  def numberOfElements(number: Long): Int = {
    var i = 0
    while(number + 1 > fact(i)) i = i + 1
    i - 1
  }

  def fact(number: Long): Long = number match {
    case 0 => 0
    case 1 => 1
    case _ => number * fact(number - 1)
  }
}
